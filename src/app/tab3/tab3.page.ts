import { Component } from '@angular/core';
import { Titled } from '../interfaces/interface';
import { DatalocalService } from '../services/datalocal.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  tituladas: Titled[] = [];

  sliderOpts={
    allowSlidePrev: false,
    allowSlideNext: false,
  }
    constructor(public datalocalService: DatalocalService) {}

}

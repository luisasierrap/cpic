export interface RespuestaProgramas {
  titled: Titled[];
  complementary: Complementary[];
}

export interface Complementary {
  id: number;
  name_program: string;
  program_type: string;
  training_modality: string;
  description: string;
  requirements: Requirement[];
  duration: string;
  image: string;
}

export interface Titled {
  id: number;
  name: string;
  duration: number;
  image: string;
  requirements?: Requirement[];
  description: string;
  requeriments?: Requirement[];
}

export interface Requirement {
  document: number;
  bachelor: string;
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplementariaComponent } from './complementaria/complementaria.component';
import { TituladaComponent } from './titulada/titulada.component';
import { ProgramasComponent } from './programas/programas.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [TituladaComponent,ComplementariaComponent,ProgramasComponent],
  imports: [
    CommonModule, IonicModule
  ],
  exports:[
    ProgramasComponent,TituladaComponent
  ]
})
export class ComponentsModule { }

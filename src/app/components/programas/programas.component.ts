import { Component, OnInit, Input } from '@angular/core';
import { Titled } from 'src/app/interfaces/interface';

@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.scss'],
})
export class ProgramasComponent implements OnInit {
  @Input() tituladas: Titled[]=[];
  @Input() Favoritos= false;
  constructor() { }

  ngOnInit() {}

}

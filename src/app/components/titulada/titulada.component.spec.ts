import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TituladaComponent } from './titulada.component';

describe('TituladaComponent', () => {
  let component: TituladaComponent;
  let fixture: ComponentFixture<TituladaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TituladaComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TituladaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

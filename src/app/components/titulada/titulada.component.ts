import { Component, OnInit,Input } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Titled } from 'src/app/interfaces/interface';
import { DatalocalService } from 'src/app/services/datalocal.service';

@Component({
  selector: 'app-titulada',
  templateUrl: './titulada.component.html',
  styleUrls: ['./titulada.component.scss'],
})
export class TituladaComponent implements OnInit {
  @Input() titulada: Titled;
  @Input() indice: number;
  @Input() Favoritos; 
  constructor(private actionSheet:ActionSheetController, private datalocalService: DatalocalService) { }


  ngOnInit() {
    console.log('Favoritos', this.Favoritos);
  }

async menu(){
  let guardarBtn;
  if(this.Favoritos){
    guardarBtn={
    text: 'Eliminar Favorito',
    icon: 'trash',
    cssClass:'action-dark',
    handler: () => {
      console.log('Eliminar de favoritos');
      this.datalocalService.eliminarPrograma(this.titulada);
    }
  };
} else {
  guardarBtn={
    text: 'Favorito',
    icon: 'star',
    cssClass: 'action-dark',
    handler:()=> {
      console.log('Favorito');
      this.datalocalService.guardarPrograma(this.titulada);
    }
  };
}
const actionSheet= await this.actionSheet.create({
  buttons: [
    {
    //   text:'Compartir',
    //   icon:'share',
    //   cssClass:'action-dark',
    //   handler: () =>{
    //     console.log('Share Clicked');
    //     this.socialSharing.share(
    //       this.titulada.name,
    //       this.titulada.name,
    //       '',
    //       this.titulada.image
    //     );
    //   }
    },
    guardarBtn,
    {
      text: 'Cancelar',
      icon: 'close',
      role: 'cancel',
      cssClass: 'action-dark',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
});
await actionSheet.present();
}
}

import { Component } from '@angular/core';
import { Titled } from '../interfaces/interface';
import { DatalocalService } from '../services/datalocal.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  tituladas: Titled[]=[];
  constructor(private datalocalService: DatalocalService){}
  slideOpts={
    slidesPerView: 1.1,
    freeMode: true
  }
  
    ngOnInit(){}
  
}

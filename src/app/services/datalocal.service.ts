import { Injectable } from '@angular/core';
import { Titled } from '../interfaces/interface';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DatalocalService {
  tituladas: Titled[] = [];
  constructor(private storage: Storage, public toastCtrl: ToastController) { 
    this.cargarFavoritos();
  }

  async presentToast(message: string){
    const toast= await this.toastCtrl.create({
      message,
      duration:1500
    });
    toast.present();
  }

  guardarPrograma(titu: Titled){
  const existe= this.tituladas.find(prog=> prog.name === titu.name)
  if(!existe){
    this.tituladas.unshift(titu);
    this.storage.set('favoritos', this.tituladas);
  }
 
  this.presentToast('Agregado a Favoritos');
}

async cargarFavoritos(){
  const favoritos= await this.storage.get('favoritos');
  if(favoritos){
    this.tituladas=favoritos;
  }
} 

eliminarPrograma(titulada: Titled){
  this.tituladas= this.tituladas.filter(prog => prog.name !==titulada.name);
  this.storage.set('favoritos', this.tituladas);
  this.presentToast('Se ha eliminado de favoritos');
}
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {RespuestaProgramas} from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class ProgramasService {

  constructor(private http: HttpClient) { }
  
  getListaProgramas(){
    return this.http.get<RespuestaProgramas>(`assets/json/programas.json`);
    }
}

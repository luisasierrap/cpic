import { Component } from '@angular/core';
import { Titled } from '../interfaces/interface';
import { ProgramasService } from '../services/programas.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  tituladas: Titled[]=[];
  constructor(private progService: ProgramasService){}
  
  
    ngOnInit(){
      this.cargarProgramas();
    }
    
    cargarProgramas(){
    this.progService.getListaProgramas()
    .subscribe(resp => {
      console.log(resp.titled);
      this.tituladas.push(...resp.titled);
    
    })
  
  }

}
